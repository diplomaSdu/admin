<h2> <b>Инструкции по запуску</b> </h2>

<p>
    <ul>
        <li>Скачиваем <a href="https://docs.docker.com/docker-for-windows/install/"> docker </a> </li>
        <li>Создаем файл <strong>.env</strong> по примеру файла <strong>.env.example</strong> в корне проекта </li>
        <li>С корня проекта запускаем команду: <br>
            -> <strong>docker-compose up --build -d </strong></li>
        <li>Заходим в php контейнер с помощью команды: <br>
           -> <strong>docker-compose exec php-cli bash</strong>
        </li>
        <li>Из под контейнера запускаем команды: <br>
            -> <strong>composer install</strong>
            <br>
            -> <strong>php init</strong>
            <br>
            -> <strong>php yii migrate</strong>
        </li>
        <li>
            Прописываем хост admin.studhunt.lcl в hosts 
        </li>
        <li>
            Заходим в http://admin.studhunt.lcl:{NGINX_OUTER_PORT} (NGINX_OUTER_PORT прописан в файле .env) 
        </li>
    </ul>
</p>

<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Advanced Project Template</h1>
    <br>
</p>

Yii 2 Advanced Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
developing complex Web applications with multiple tiers.

The template includes three tiers: front end, back end, and console, each of which
is a separate Yii application.

The template is designed to work in a team development environment. It supports
deploying the application in different environments.

Documentation is at [docs/guide/README.md](docs/guide/README.md).

[![Latest Stable Version](https://img.shields.io/packagist/v/yiisoft/yii2-app-advanced.svg)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![Total Downloads](https://img.shields.io/packagist/dt/yiisoft/yii2-app-advanced.svg)](https://packagist.org/packages/yiisoft/yii2-app-advanced)
[![build](https://github.com/yiisoft/yii2-app-advanced/workflows/build/badge.svg)](https://github.com/yiisoft/yii2-app-advanced/actions?query=workflow%3Abuild)

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
